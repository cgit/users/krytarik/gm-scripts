// ==UserScript==
// @name        uftow-stats-simple
// @namespace   uftow-stats-simple
// @include     https://ubuntuforums.org/showthread.php?t=1579442*
// @version     1.4.0
// @Description Generates stats for Tug of War - simple variant.
// @grant		GM_log
// @grant		GM_getValue
// @grant		GM_setValue
// @grant		GM_listValues
// @grant		GM_deleteValue
// @grant		GM_registerMenuCommand
// ==/UserScript==

var towStats = {
	alreadyHasStartCounting:false,
	scores:new Array(),
	lastPostNumber:0,
	lastPoster:null,
	startPostNumber:null,
	endPostNumber:null,
	init: function(){
		this.postContainers=document.getElementsByClassName('postcontainer');
		this.firstPostNumber=parseInt(this.postContainers[0].getElementsByClassName('postcounter')[0].textContent.substr(1));
		this.lastPostNumber=parseInt(this.postContainers[this.postContainers.length-1].getElementsByClassName('postcounter')[0].textContent.substr(1));
		if(!GM_getValue("statsActive",false)){
			this.addStartCounting();
		}else{
			this.continueCounting();
		}
		GM_registerMenuCommand("Tug of War: Clear counter",function(){towStats.clearStats();},'A');
	},
	addStartCounting: function(){
		GM_registerMenuCommand("Tug of War: Start counting",function(){towStats.startCounting();},'S');
		GM_registerMenuCommand("Tug of War: Continue counting",function(){towStats.continueCounting();},'C');
		GM_registerMenuCommand("Tug of War: Show last results",function(){towStats.showSummary();},'R');
		this.alreadyHasStartCounting=true;
	},
	recordPosts: function(startPost){
		var postNumber;
		var startDiff=startPost-this.firstPostNumber;
		var postInfo;
		if(startDiff<0 || startPost>this.firstPostNumber+this.postContainers.length-1){
			alert("Post #"+startPost+" is not on this page.");
			return false;
		}
		for (var i=startDiff;i<this.postContainers.length;i++){
			postInfo=this.getPostInfo(this.postContainers[i]);
			this.lastPostNumber=postInfo[1];
			if(this.lastPoster==postInfo[0]){
				//GM_log("Post #"+this.lastPostNumber+" is a duplicate and has been skipped.");
				continue;
			}
			if(this.endPostNumber!=null && this.lastPostNumber>this.endPostNumber){
				break;
			}
			this.scorePost(postInfo);
		}
		return true;
	},
	getPostInfo: function(postElement){
		var userSpanTag=postElement.getElementsByClassName('username')[0].getElementsByTagName('span')[0];
		var userName=userSpanTag.textContent;
		var postNumber=parseInt(postElement.getElementsByClassName('postcounter')[0].textContent.substr(1));
		return new Array(userName,postNumber);
	},
	scorePost: function(postInfo){
		this.lastPoster=postInfo[0];
		userIndex=this.scores.indexOf(postInfo[0]);
		//The second check in the if statement makes sure users with usernames being actual numbers don't break this script
		if(userIndex!=-1&&userIndex%2==0){
			this.scores[userIndex+1]++;
		}else{
			this.scores.push(postInfo[0]);
			this.scores.push(1);
		}
	},
	getSummary: function(){
		if(this.scores.length>=2 && !(this.endPostNumber!=null && this.lastPostNumber<this.endPostNumber)){
			window.alert("Counting complete.\n"+
				"Stats will be displayed in the next dialog box.");
		}else{
			window.alert("Tug of War is yet to be completed.");
			return false;
		}
		var alertText="[B]TUG OF WAR LAST ROUND STATS[/B]\n";
		var totalParticipants=0;
		var totalPosts=0;
		for(var i=1;i<this.scores.length;i+=2){
			//GM_log("STATS: User: "+this.scores[i-1]+", Posts: "+this.scores[i]);
			totalParticipants++;
			totalPosts+=this.scores[i];
		}
		alertText+="[B]Post range:[/B] "+this.startPostNumber+" - "+
			((this.endPostNumber!=null)?this.endPostNumber:this.lastPostNumber)+"\n";
		alertText+="[B]Post count:[/B] "+totalPosts+"\n";
		alertText+="[B]Participants:[/B] "+totalParticipants+"\n\n";
		alertText+="[B]PARTICIPANTS[/B]";
		alertText+=this.getListText(this.scores);
		window.alert(alertText);
	},
	getListText: function(participants){
		var statsText="";
		var thisScore;
		var currScore=0;
		var reCheck=true;
		var tempSpot;
		while(reCheck){
			reCheck=false;
			for(var i=0;i<participants.length;i+=2){
				if(i+2>=participants.length){
					break;
				}
				if(participants[i+1]<participants[i+3]){
					tempSpot=new Array(participants[i],participants[i+1]);
					participants[i]=participants[i+2];
					participants[i+1]=participants[i+3];
					participants[i+2]=tempSpot[0];
					participants[i+3]=tempSpot[1];
					reCheck=true;
				}
			}
		}
		//List individually
		//for(var i=0;i<participants.length;i+=2){
		//	thisScore=participants[i+1];
		//	statsText+="[B]"+participants[i]+":[/B] "+participants[i+1]+" ";
		//	statsText+="post"+((thisScore!=1)?"s":"");
		//	statsText+="\n";
		//}
		//Group by post count
		for(var i=0;i<participants.length;i+=2){
			thisScore=participants[i+1];
			if(thisScore!=currScore){
				currScore=thisScore;
				statsText+="\n[B]"+thisScore+" post"+((thisScore!=1)?"s":"")+":[/B] "+participants[i];
			}else{
				statsText+=", "+participants[i];
			}
		}
		if(participants.length==0){
			statsText+="None\n";
		}
		return new Array(statsText);
	},
	goToNextPage: function(){
		var pageLinks=document.getElementsByClassName('pagination_top')[0];
		var nextLinkContainer=pageLinks.getElementsByClassName('prev_next')[1];
		if(nextLinkContainer!=undefined){
			nextLinkContainer.getElementsByTagName('a')[0].click();
			return true;
		}else{
			return false;
		}
	},
	startCounting: function(){
		this.clearStats();
		answerStart=window.prompt("Specifiy a post number to begin counting.\n"+
			"If you do not know the post number, click Cancel.");
		if(answerStart!=null){
			startPost=parseInt(answerStart);
			this.startPostNumber=startPost;
			answerEnd=window.prompt("Specifiy a post number to end counting.\n"+
				"If you want to count until the end, click Cancel.");
			if(answerEnd!=null){
				this.endPostNumber=parseInt(answerEnd);
			}
			GM_setValue("statsActive",true);
			this.doCounting(startPost);
		}
	},
	continueCounting: function(){
		if(this.restoreStats()){
			startPost=this.lastPostNumber+1;
			GM_setValue("statsActive",true);
			this.doCounting(startPost);
		}
	},
	doCounting: function(startPost){
		if(!this.recordPosts(startPost)){
			this.endCounting();
			return false;
		}
		this.saveStats();
		if((this.endPostNumber!=null && this.lastPostNumber>=this.endPostNumber) || !this.goToNextPage()){
			this.getSummary();
			this.endCounting();
		}
	},
	endCounting: function(){
		GM_setValue("statsActive",false);
		if(!this.alreadyHasStartCounting){
			this.addStartCounting();
		}
	},
	showSummary: function(){
		if(this.restoreStats()){
			this.getSummary();
		}
	},
	saveStats: function(){
		//GM_log("Last post on this page: "+this.lastPostNumber);
		GM_setValue("lastPostNumber",this.lastPostNumber);
		for(i=0;i<this.scores.length;i++){
			GM_setValue("scores."+i,this.scores[i]);
			GM_setValue("scores.count",this.scores.length);
		}
		GM_setValue("lastPoster",this.lastPoster);
		GM_setValue("startPostNumber",this.startPostNumber);
		GM_setValue("endPostNumber",this.endPostNumber);
		GM_setValue("statsCached",true);
	},
	restoreStats: function(){
		if(!GM_getValue("statsCached",false)){
			window.alert("No cached stats.")
			return false;
		}
		var scoresCount=GM_getValue("scores.count",0);
		this.lastPoster=GM_getValue("lastPoster",null);
		this.lastPostNumber=parseInt(GM_getValue("lastPostNumber"));
		this.startPostNumber=parseInt(GM_getValue("startPostNumber"));
		this.endPostNumber=GM_getValue("endPostNumber");
		if(this.endPostNumber!=null){
			this.endPostNumber=parseInt(this.endPostNumber);
		}
		this.scores=new Array();
		for(i=0;i<scoresCount;i++){
			this.scores.push(GM_getValue("scores."+i));
		}
		return true;
	},
	clearStats: function(){
		this.scores=new Array();
		this.lastPostNumber=0;
		this.lastPoster=null;
		this.startPostNumber=null;
		this.endPostNumber=null;
		keys=GM_listValues();
		for (var i=0,key=null; key=keys[i];i++) {
			GM_deleteValue(key);
		}
	}
};

towStats.init();
